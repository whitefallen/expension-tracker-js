const balance = document.getElementById('balance');
const moneyIncome = document.getElementById('moneyIncome');
const moneyExpense = document.getElementById('moneyExpense');
//const list = document.getElementById('list');
const form = document.getElementById('form');
const description = document.getElementById('description');
const amount = document.getElementById('amount');
const date = document.getElementById('date');
const historyContainer = document.getElementById('historyContainer');

const localStorageTransactions = JSON.parse(
    localStorage.getItem('transactions')
);

let transactionStorage = localStorage.getItem('transactions') !== null ? localStorageTransactions : [];

// Generate random ID
function generateId() {
    return Math.floor(Math.random() * 100000000);
}

function addToDOM(newContainer, monthDate) {
    let searchParent;
    let index;
    for(index = monthDate; index >= 0; index--) {
        searchParent = document.getElementById('month_'+index);
        if(searchParent) {
            break;
        }
    }
    if(searchParent) {
        if(index < monthDate) {
            searchParent.before(newContainer);
        } else {
            searchParent.after(newContainer);
        }
    } else {
        historyContainer.append(newContainer);
    }
}

function prepareMonthSection(transaction) {
    let monthList;
    const monthDate = moment(transaction.date, 'DD.MM.YYYY').month();
    let monthContainer = document.getElementById('month_'+monthDate);
    if(!monthContainer) {
        const newContainer = document.createElement('div');
        newContainer.id = 'month_' + monthDate;
        newContainer.innerText = moment().month(monthDate).format('MMMM');

        monthList = document.createElement('ul');
        monthList.className = 'list';
        monthList.id = 'list_month_'+monthDate;

        newContainer.appendChild(monthList);
        addToDOM(newContainer, monthDate);
    } else {
        monthList = document.getElementById('list_month_'+monthDate);
    }
    return monthList;
}

function sortTransactionInList(listDOM) {
    const listChilds = listDOM.children;
    const sortedList = Array.from(listChilds).sort((compareObj1, compareObj2) => {
        if(moment(compareObj1.dataset.date, 'DD.MM.YYYY') > moment(compareObj2.dataset.date, 'DD.MM.YYYY') ) {
            return -1;
        }
        if(moment(compareObj1.dataset.date, 'DD.MM.YYYY') < moment(compareObj2.dataset.date, 'DD.MM.YYYY')) {
            return 1;
        }
        return 0;
    });
    listDOM.innerHTML = '';
    sortedList.forEach(listElement => listDOM.appendChild(listElement));
}

// Display Transaction in DOM
function displayTransaction(transaction) {
    const monthList = prepareMonthSection(transaction);
    const transactionDOM = document.createElement('li');
    let transactionSign = '';
    if(transaction.amount < 0) {
        transactionSign = '-';
        transactionDOM.classList.add('minus');
    } else {
        transactionSign = '+';
        transactionDOM.classList.add('plus');
    }
    transactionDOM.innerHTML = `
        ${transaction.text} - ${transaction.date} <span>${transactionSign} ${Math.abs(transaction.amount).toFixed(2)}</span>
        <button class="delete-btn" onclick="deleteTransaction(${transaction.id})">x</button>
    `;
    transactionDOM.dataset.date = transaction.date;
    monthList.appendChild(transactionDOM);
    sortTransactionInList(monthList);

}

// Update Income/Expense History
function updateValues() {
    const historyAmount = transactionStorage.map(transaction => transaction.amount);
    const historyTotal = historyAmount.reduce((totalSum,transactionAmount) => (totalSum += transactionAmount), 0).toFixed(2);
    const income = historyAmount.filter(transactionAmount => transactionAmount > 0).reduce((totalSum, transactionAmount) => (totalSum += transactionAmount), 0 ).toFixed(2);
    const expense = historyAmount.filter(transactionAmount => transactionAmount < 0).reduce((totalSum, transactionAmount) => (totalSum += transactionAmount), 0 ).toFixed(2);
    balance.innerText = `${historyTotal}€`;
    moneyIncome.innerText = `${income}€`;
    moneyExpense.innerText = `${expense}€`;
}

// Init app
function init() {
    historyContainer.innerHTML = '';
    transactionStorage.forEach(displayTransaction);
    updateValues();
}

// Update the LocalStorage
function updateLocalStorage() {
    localStorage.setItem('transactions', JSON.stringify(transactionStorage));
}

// Delete a Transaction by Id
function deleteTransaction(transactionId) {
    transactionStorage = transactionStorage.filter(transaction => transaction.id !== transactionId);
    updateLocalStorage();
    init();
}

// Reset the inputs in the DOM
function resetTransactionInput() {
    amount.value = '';
    description.value = '';
    date.value = '';
}

// Add Transction to LocalStorage und update/display
function addTransaction(event) {
    event.preventDefault();
    if(description.value.trim() === '' || amount.value.trim() === '' || date.value.trim() === '') {
        alert('Please add a text, amount and date');
    } else {
        const transaction = {
            id: generateId(),
            text: description.value.trim(),
            // the '+' is a Unary Operator that tries to convert to number
            // https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Operators/Arithmetic_Operators#Un%C3%A4res_Plus
            amount: +amount.value.trim(),
            date: moment(date.value.trim()).format('DD.MM.YYYY')
        };

        transactionStorage.push(transaction);

        displayTransaction(transaction);

        updateValues();

        updateLocalStorage();

        resetTransactionInput();
    }
}

init();

form.addEventListener('submit', addTransaction);
